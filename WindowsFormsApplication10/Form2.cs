﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication10
{
    public partial class Form2 : Form
    {
       
        DataSet data = new DataSet();
        SqlConnection connect = new SqlConnection("Data Source =ASUS\\SQLEXPRESS; Initial Catalog =Rosliny; Integrated Security = True");
        SqlDataAdapter dataadapter = new SqlDataAdapter();
        BindingSource MyTablefromSQL = new BindingSource();
        public Form2()
        {
            
            InitializeComponent();
            try
            {
                
                data.Clear();
                dataadapter.SelectCommand = new SqlCommand("select * from Sklep where Dostepnosc=0", connect);
                dataadapter.Fill(data);
                dataGrid.DataSource = data.Tables[0];
                MyTablefromSQL.DataSource = data.Tables[0];
               // textNazwa.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Nazwa"));
            }
            catch (ArgumentException )
            {
                DialogResult x;
                x = MessageBox.Show("Wysapil blad, nastapi ponowne uruchomienie aplikacji.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.Exit();
                Application.Restart();
            }
           
        }
               
      

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
    }
}
