﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Timers;
using System.Data.SqlTypes;
using System.Threading;

namespace WindowsFormsApplication10
{
    
    public partial class Form1 : Form
    {
        DataSet data = new DataSet();
        SqlConnection connect = new SqlConnection("Data Source =ASUS\\SQLEXPRESS; Initial Catalog =Rosliny; Integrated Security = True");
        SqlDataAdapter dataadapter = new SqlDataAdapter();
        BindingSource MyTablefromSQL = new BindingSource();
       

        public Form1()
        {          
            InitializeComponent();            
        }
//grafika
        float dx = 0;
        private void Form1_Load(object sender, EventArgs e)
        {            
            this.Paint += Form1_Paint;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            dx += 1.05f;
            this.Refresh();
        }    
    
        private void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {           
            mouses(e, Brushes.LimeGreen, 650, -50, 15, 5, Pens.LimeGreen);
            mouses(e, Brushes.LimeGreen, 650, 50, 15, 5, Pens.LimeGreen);
            mouses(e, Brushes.LimeGreen, 650, 150, 15, 5, Pens.LimeGreen);
            mouses(e, Brushes.LimeGreen, 650, 250, 15, 5, Pens.LimeGreen);       

            e.Graphics.TranslateTransform(-dx, dx);
            mouses(e, Brushes.LimeGreen, 650, 350, 15, 5, Pens.LimeGreen);
            mouses(e, Brushes.LimeGreen, 550, 350, 15, 5, Pens.LimeGreen);
            e.Graphics.TranslateTransform(dx, -dx);
            mouses(e, Brushes.LimeGreen, 50, 350, 15, 5, Pens.LimeGreen);

        }
        private void mouses(System.Windows.Forms.PaintEventArgs e,Brush a, int y1, int y2, int y3, int y4,Pen b)
        {
            Graphics mouse = e.Graphics;
            mouse.TranslateTransform(0, -dx);
            mouse.FillEllipse(a, y1, y2, y3, y3);
            mouse.FillEllipse(a, y1 - 2, y2 - 2, y4, y4);
            mouse.FillEllipse(a, y1 + 6, y2 - 2, y4, y4);
            mouse.DrawLine(b, y1 + 10, y2 + 10, y1 + 13, y2 + 21);
        }
/**/
        public delegate void MyDelegateColor(object sender, EventArgs e);
        public event MyDelegateColor ChangeData;       

       public delegate void MyDelegate();
       private void GetDelegate()
       {
           MyDelegate help = new MyDelegate(Up);
           help += ClearAddedThings;
           help();
        }
       private void Color_MouseClick(Object sender, MouseEventArgs e)
       {
           ChangeData += new MyDelegateColor(dataChangeColor);
           Thread wyjatek = new Thread(() => ChangeData(sender, e));           
           wyjatek.IsBackground = true;
           wyjatek.Start();
          
       }
        
       private void dataChangeColor(object sender, EventArgs e)
       {         
               if (dataGrid.SelectedRows != null)
                   for (int i = 0; i < (dataGrid.Rows.Count) - 1; i++)
                   {
                       if (Convert.ToBoolean(this.dataGrid.CurrentRow.Cells["Dostepnosc"].Value) == true)
                           if (Convert.ToInt32(dataGrid.Rows[i].Cells["Dostepnosc"].Value) == 0)
                               dataGrid.Rows[i].DefaultCellStyle.BackColor = Color.Thistle;
                   }          
       }

       private void DataGrid_ClickSelect(object sender, DataGridViewCellMouseEventArgs e)
       {
           if (e.Button == MouseButtons.Left)
           {
               int selected = e.RowIndex;
               if (e.RowIndex != -1)
               {
                   this.dataGrid.Rows[selected].Selected = true;

                   MyTablefromSQL.Position = selected;
                   cleardataTextboxes();                
                   showdataTextboxes();                   
               }
           }
       }

       private void cleardataTextboxes()
       {
           textNazwa.DataBindings.Clear();
           textCena.DataBindings.Clear();
           textDostepnosc.DataBindings.Clear();
           KategoriaListBox1.DataBindings.Clear();
           TypListBox2.DataBindings.Clear();
       } 
    
        private void showdataTextboxes()
       {
           textNazwa.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Nazwa"));
           textCena.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Cena"));
           textDostepnosc.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Dostepnosc"));
           KategoriaListBox1.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Kategoria"));
           TypListBox2.DataBindings.Add(new Binding("Text", MyTablefromSQL, "Typ"));      
           
       }
        
        
        private void dane_Click(object sender, EventArgs e)
        {  
            try
            {    
                dataadapter.SelectCommand = new SqlCommand("select * from Sklep", connect);
                dataadapter.Fill(data);
                dataGrid.DataSource = data.Tables[0];
                MyTablefromSQL.DataSource = data.Tables[0];
                showdataTextboxes();
            }

            catch (ArgumentException)
            {
                exceptions();
            }          
          
        }      


        private void braki_Click(object sender, EventArgs e)
        {       
            Form2 form2 = new Form2();
            form2.Show();            
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;
                dataadapter.UpdateCommand = new SqlCommand("update dbo.Sklep set Nazwa=@Nazwa,Kategoria=@Kategoria,Cena=@Cena,Dostepnosc=@Dostepnosc,Typ=@Typ where idSklep=@idSklep", connect);

                dataadapter.UpdateCommand.Parameters.Add("@Nazwa", SqlDbType.VarChar).Value = textNazwa.Text;
                dataadapter.UpdateCommand.Parameters.Add("@Kategoria", SqlDbType.VarChar).Value = KategoriaListBox1.SelectedItem.ToString();

                decimal cena = Convert.ToDecimal(textCena.Text);
                dataadapter.UpdateCommand.Parameters.Add("@Cena", SqlDbType.Money).Value = cena;

                int dostepnosc = Convert.ToInt32(textDostepnosc.Text);
                dataadapter.UpdateCommand.Parameters.Add("@Dostepnosc", SqlDbType.Int).Value = dostepnosc;
                dataadapter.UpdateCommand.Parameters.Add("@Typ", SqlDbType.VarChar).Value = TypListBox2.SelectedItem.ToString();
                dataadapter.UpdateCommand.Parameters.Add("@idSklep", SqlDbType.Int).Value = data.Tables[0].Rows[MyTablefromSQL.Position][0];
                index = MyTablefromSQL.Position;

                connect.Open();
                dataadapter.UpdateCommand.ExecuteNonQuery();
                connect.Close();
                
                GetDelegate();
                AutoScrolling(index);             
                                        
            }
            catch (SystemException)
            {
                exceptions();
            }
        }


        private void AddRecord_Click(object sender, EventArgs e)
        {
            try
            {
                dataadapter.InsertCommand = new SqlCommand("insert into dbo.Sklep(Nazwa,Kategoria,Typ,Cena,Dostepnosc)values(@Nazwa,@Kategoria,@Typ,@Cena,@Dostepnosc)", connect);

                dataadapter.InsertCommand.Parameters.Add("@Nazwa", SqlDbType.VarChar).Value = textNazwa.Text;
                dataadapter.InsertCommand.Parameters.Add("@Kategoria", SqlDbType.VarChar).Value = KategoriaListBox1.SelectedItem.ToString();
                dataadapter.InsertCommand.Parameters.Add("@Typ", SqlDbType.VarChar).Value = TypListBox2.SelectedItem.ToString();

                decimal cena = Convert.ToDecimal(textCena.Text);
                dataadapter.InsertCommand.Parameters.Add("@Cena", SqlDbType.Money).Value = cena;

                int dostepnosc = Convert.ToInt32(textDostepnosc.Text);
                dataadapter.InsertCommand.Parameters.Add("@Dostepnosc", SqlDbType.VarChar).Value = dostepnosc;

                connect.Open();
                dataadapter.InsertCommand.ExecuteNonQuery();
                connect.Close();

                GetDelegate();
                Last(sender, e);
             
            }
            catch (SystemException)
            {
                exceptions();
            }
        }        

        private void Clear_Selecton()
        {
            dataGrid.ClearSelection();
            dataGrid.Rows[MyTablefromSQL.Position].Selected = true;
        }

        private void Up()
        {
            try
            {
                data.Clear();
                dataadapter.Fill(data);
                dataGrid.ClearSelection();
                dataGrid.Rows[MyTablefromSQL.Position].Selected = true;
            }
            catch (SystemException)
            {
                exceptions();
            }

        }
             

        private void Exit_Click(object sender, EventArgs e)
        {            
            Application.Exit();
        }
       
        private void First_Click(object sender, EventArgs e)
        {
            try
            {
                MyTablefromSQL.MoveFirst();
                Clear_Selecton();
                dataGrid.FirstDisplayedScrollingRowIndex = 0;
            }
            catch (SystemException) { exceptions(); }
        }

        private void Previous(object sender, EventArgs e)
        {
            try
            {
                MyTablefromSQL.MovePrevious();
                Clear_Selecton();
                if (MyTablefromSQL.Position<=dataGrid.RowCount -5)
                    AutoScrolling(MyTablefromSQL.Position);
            }
            catch (SystemException) { exceptions(); }
        }

        private void Next(object sender, EventArgs e)
        {
            try
            {
                MyTablefromSQL.MoveNext();
                Clear_Selecton();
                if (MyTablefromSQL.Position > 4)
                    AutoScrolling((MyTablefromSQL.Position) - 4);
            }
            catch (SystemException) { exceptions(); }
        }

        private void Last(object sender, EventArgs e)
        {
            try
            {
                MyTablefromSQL.MoveLast();
                Clear_Selecton();
                AutoScrolling((dataGrid.Rows.Count) - 1);
            }
            catch (SystemException) { exceptions(); }
        }
    
    

        private void AutoScrolling(int index)
        {
            dataGrid.FirstDisplayedScrollingRowIndex = index;
        }

        private void ClearAddedThings()
        {
            textDostepnosc.Clear();
            textNazwa.Clear();
            textCena.Clear();
            
            foreach (int i in TypListBox2.CheckedIndices)
                TypListBox2.SetItemCheckState(i, CheckState.Unchecked);
            
            foreach (int i in KategoriaListBox1.CheckedIndices)
                 KategoriaListBox1.SetItemCheckState(i, CheckState.Unchecked);                
           
        }
       
        private void delete(object sender, EventArgs e)
        {
            try
            {
                DialogResult x;
                x = MessageBox.Show("Czy na pewno chcesz usunac wybrany element?", "Usuniecie pola", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (x == DialogResult.Yes)
                {
                    dataadapter.DeleteCommand = new SqlCommand("Delete from dbo.Sklep where Nazwa=@Nazwa", connect);
                    dataadapter.DeleteCommand.Parameters.Add("@Nazwa", SqlDbType.VarChar).Value = data.Tables[0].Rows[MyTablefromSQL.Position][1];

                    connect.Open();
                    dataadapter.DeleteCommand.ExecuteNonQuery();
                    connect.Close();
                }
                Up();               
            }
            catch (IndexOutOfRangeException)
            { exceptions(); }   
                          
        }

        private void Update_Click(object sender, EventArgs e)
        {
            Up();           
        }

        private void exceptions()
        {
            DialogResult x;
            x = MessageBox.Show("Wystapił błąd","Error", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            Application.Exit();
            Application.Restart();
        }   
    }
}


