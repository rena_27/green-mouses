﻿namespace WindowsFormsApplication10
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.roslinyDataSet = new WindowsFormsApplication10.RoslinyDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.roslinyDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.typProduktuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.typProduktuTableAdapter = new WindowsFormsApplication10.RoslinyDataSetTableAdapters.TypProduktuTableAdapter();
            this.textNazwa = new System.Windows.Forms.TextBox();
            this.textCena = new System.Windows.Forms.TextBox();
            this.KategoriaListBox1 = new System.Windows.Forms.CheckedListBox();
            this.TypListBox2 = new System.Windows.Forms.CheckedListBox();
            this.Zakoncz = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.Odswiez = new System.Windows.Forms.Button();
            this.textDostepnosc = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.roslinyDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roslinyDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typProduktuBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // roslinyDataSet
            // 
            this.roslinyDataSet.DataSetName = "RoslinyDataSet";
            this.roslinyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(533, 219);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 75);
            this.button1.TabIndex = 2;
            this.button1.Text = "Pobierz Dane";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.dane_Click);
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // dataGrid
            // 
            this.dataGrid.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGrid.Location = new System.Drawing.Point(12, 12);
            this.dataGrid.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(621, 150);
            this.dataGrid.TabIndex = 3;
            this.dataGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGrid_ClickSelect);
            // 
            // roslinyDataSetBindingSource
            // 
            this.roslinyDataSetBindingSource.DataSource = this.roslinyDataSet;
            this.roslinyDataSetBindingSource.Position = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(427, 219);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 38);
            this.button2.TabIndex = 4;
            this.button2.Text = "Wyświetl braki w magazynie";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.braki_Click);
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(118, 357);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(76, 38);
            this.button3.TabIndex = 5;
            this.button3.Text = "Edytuj";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Edit_Click);
            this.button3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 357);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 38);
            this.button4.TabIndex = 6;
            this.button4.Text = "Dodaj rekord";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.AddRecord_Click);
            this.button4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 213);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nazwa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 244);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Cena";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 272);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Dostępność";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(200, 213);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Kategoria:";
            this.label4.UseCompatibleTextRendering = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(184, 281);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Typ produktu:";
            // 
            // typProduktuBindingSource
            // 
            this.typProduktuBindingSource.DataMember = "TypProduktu";
            this.typProduktuBindingSource.DataSource = this.roslinyDataSetBindingSource;
            // 
            // typProduktuTableAdapter
            // 
            this.typProduktuTableAdapter.ClearBeforeFill = true;
            // 
            // textNazwa
            // 
            this.textNazwa.Location = new System.Drawing.Point(64, 210);
            this.textNazwa.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textNazwa.Name = "textNazwa";
            this.textNazwa.Size = new System.Drawing.Size(130, 20);
            this.textNazwa.TabIndex = 14;
            // 
            // textCena
            // 
            this.textCena.Location = new System.Drawing.Point(64, 241);
            this.textCena.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textCena.Name = "textCena";
            this.textCena.Size = new System.Drawing.Size(130, 20);
            this.textCena.TabIndex = 15;
            // 
            // KategoriaListBox1
            // 
            this.KategoriaListBox1.FormattingEnabled = true;
            this.KategoriaListBox1.Items.AddRange(new object[] {
            "Kwiaty",
            "Warzywa",
            "Owoce",
            "Ziola"});
            this.KategoriaListBox1.Location = new System.Drawing.Point(262, 211);
            this.KategoriaListBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.KategoriaListBox1.Name = "KategoriaListBox1";
            this.KategoriaListBox1.Size = new System.Drawing.Size(120, 64);
            this.KategoriaListBox1.TabIndex = 16;
            // 
            // TypListBox2
            // 
            this.TypListBox2.FormattingEnabled = true;
            this.TypListBox2.Items.AddRange(new object[] {
            "sadzonka",
            "nasiona"});
            this.TypListBox2.Location = new System.Drawing.Point(262, 281);
            this.TypListBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TypListBox2.Name = "TypListBox2";
            this.TypListBox2.Size = new System.Drawing.Size(120, 34);
            this.TypListBox2.TabIndex = 17;
            // 
            // Zakoncz
            // 
            this.Zakoncz.Location = new System.Drawing.Point(530, 346);
            this.Zakoncz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Zakoncz.Name = "Zakoncz";
            this.Zakoncz.Size = new System.Drawing.Size(103, 49);
            this.Zakoncz.TabIndex = 19;
            this.Zakoncz.Text = "Zakończ";
            this.Zakoncz.UseVisualStyleBackColor = true;
            this.Zakoncz.Click += new System.EventHandler(this.Exit_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button5.Location = new System.Drawing.Point(104, 166);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(64, 25);
            this.button5.TabIndex = 20;
            this.button5.Text = "|<<";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.First_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button6.Location = new System.Drawing.Point(442, 166);
            this.button6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(64, 25);
            this.button6.TabIndex = 21;
            this.button6.Text = ">>|";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Last);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button7.Location = new System.Drawing.Point(318, 166);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(64, 25);
            this.button7.TabIndex = 22;
            this.button7.Text = ">";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Next);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button8.Location = new System.Drawing.Point(222, 166);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(64, 25);
            this.button8.TabIndex = 23;
            this.button8.Text = "<";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Previous);
            // 
            // button9
            // 
            this.button9.AllowDrop = true;
            this.button9.Location = new System.Drawing.Point(222, 357);
            this.button9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(88, 38);
            this.button9.TabIndex = 24;
            this.button9.Text = "Usuń rekord";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.delete);
            this.button9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // Odswiez
            // 
            this.Odswiez.Location = new System.Drawing.Point(427, 270);
            this.Odswiez.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Odswiez.Name = "Odswiez";
            this.Odswiez.Size = new System.Drawing.Size(88, 27);
            this.Odswiez.TabIndex = 25;
            this.Odswiez.Text = "Odśwież";
            this.Odswiez.UseVisualStyleBackColor = true;
            this.Odswiez.Click += new System.EventHandler(this.Update_Click);
            this.Odswiez.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Color_MouseClick);
            // 
            // textDostepnosc
            // 
            this.textDostepnosc.Location = new System.Drawing.Point(88, 270);
            this.textDostepnosc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textDostepnosc.Name = "textDostepnosc";
            this.textDostepnosc.Size = new System.Drawing.Size(80, 20);
            this.textDostepnosc.TabIndex = 26;
            this.textDostepnosc.Text = "";
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(707, 427);
            this.Controls.Add(this.textDostepnosc);
            this.Controls.Add(this.Odswiez);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Zakoncz);
            this.Controls.Add(this.TypListBox2);
            this.Controls.Add(this.KategoriaListBox1);
            this.Controls.Add(this.textCena);
            this.Controls.Add(this.textNazwa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "Zielone myszki";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.roslinyDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roslinyDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typProduktuBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RoslinyDataSet roslinyDataSet;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.BindingSource roslinyDataSetBindingSource;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource typProduktuBindingSource;
        private RoslinyDataSetTableAdapters.TypProduktuTableAdapter typProduktuTableAdapter;
        private System.Windows.Forms.TextBox textNazwa;
        private System.Windows.Forms.TextBox textCena;
        private System.Windows.Forms.CheckedListBox KategoriaListBox1;
        private System.Windows.Forms.CheckedListBox TypListBox2;
        private System.Windows.Forms.Button Zakoncz;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button Odswiez;
        private System.Windows.Forms.RichTextBox textDostepnosc;
    }
}

